# Le traduzioni di PeaceLink

Qui trovate le cartelle con le lingue di partenza delle traduzioni (per esempio **en**), pronte per essere inserite (con un copia/incolla, o poco più) in PhPeace, il nostro software di gestione articoli e traduzioni.

Il testo può essere modificato e ogni modifica può essere approvata, oppure annullata (si conserva una cronologia).

Facciamo una prova?

