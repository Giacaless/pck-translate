# Eric Schmidt, who led Google's transformation into a tech giant, has left the company
## Schmidt exited as a technical advisor at Google parent company Alphabet in February

> Autore: [Richard Nieva](https://www.cnet.com/profiles/richardnieva/)
> 
> Link: https://www.cnet.com/news/eric-schmidt-who-led-googles-transformation-into-a-tech-giant-has-left-the-company/
>
> Data: May 9, 2020 10:23 a.m.
>
> Traduzione: Giacomo Alessandroni
>
> Revisione: --
>
> Adattamento: --
>
> Link PeaceLink: https://www.peacelink.it/diritto/a/47642.html

***

> ![google-hq-sede-mountain-view.jpg](https://cnet1.cbsistatic.com/img/vO2bLvjQpHprcPU9hzAAM-p5rZ4=/2017/03/08/c4644171-0fb9-447b-a95c-54b664f33529/eric-schmidt-google-cloud-next-2017-7902.jpg)
>
> Text: Former Google CEO Eric Schmidt is no longer a technical advisor at the company.
>
> Source: James Martin/CNET                                                

[Eric Schmidt](https://www.cnet.com/tags/eric-schmidt/), who drove [Google's](https://www.cnet.com/google/) transformation from Silicon Valley start-up to global titan, is no  longer an adviser to the search giant and its parent Alphabet, marking  another milestone in recent personnel shake-ups that have seen the  company's old guard bow out. 

Schmidt,  tapped as Google's CEO in 2001, left his role as a technical advisor in  February, according to a person familiar with the situation. His exit  ends a 19-year tenure at Google, where he was brought in to be the  "adult supervision" to the company's young founders, Larry Page and  Sergey Brin. Schmidt's departure comes three years after Schmidt said he was [stepping down as executive chairman](https://www.cnet.com/news/eric-schmidt-to-step-down-as-executive-chairman-of-alphabet/) and would no longer serve in an operational role. 

Representatives for Schmidt and Google declined to comment. 

Schmidt's exit marks another stage in Google's evolution, and comes as Schmidt's  participation in government projects has raised questions about  conflicts of interest. Late last year, Page and Brin, who started Google as Stanford University grad students in 1998, [handed leadership](https://www.cnet.com/news/sundar-pichai-becomes-ceo-of-alphabet-googles-parent-company/) of Alphabet and its sprawling operations to Sundar Pichai, who had been running the core search business since 2015. David Drummond, the  company's 14-year legal chief, [retired in January](https://www.cnet.com/news/alphabet-legal-chief-david-drummond-is-leaving-the-company/) after scrutiny over past relationships. 

As the original management departs, employees and industry observers have  questioned whether the world's largest search engine, with more than  120,000 employees around the globe, can maintain its famously  freewheeling culture. In the past three years, tensions between  management and employees have mounted over the handling of sexual  misconduct allegations directed at top executives, a censored search  engine project in China and initiatives around [artificial intelligence](https://www.cnet.com/tags/artificial-intelligence/) for the US Department of Defense. 

>  ![gettyimages-457954872](https://cnet4.cbsistatic.com/img/cuHeK2relDQ6TJM81L5ZToHcdcE=/644x0/2020/05/09/35dd539f-2a44-4e7a-bb60-ef446ef45fd1/gettyimages-457954872.jpg)
>
> Text: Schmidt with New York Gov. Andrew Cuomo (center) in 2014.
>
> Source: Getty                                                

Schmidt's role at Google had gradually diminished after he stepped down as CEO in 2011. Still, his ties to the company have spurred blow-back as Schmidt  increases his [work on US military initiatives](https://www.nytimes.com/2020/05/02/technology/eric-schmidt-pentagon-google.html). He chairs the Defense Innovation Board, an advisory group aimed at  bringing new technology to the Pentagon, including advancements in  machine learning. He's also chairman of the National Security Commission on Artificial Intelligence, which advises Congress on AI for defense.  Critics, though, worry Schmidt could unfairly [push Google's financial interests](https://www.propublica.org/article/how-amazon-and-silicon-valley-seduced-the-pentagon) when it comes to his work with the military. 

Earlier this week, New York Gov. Andrew Cuomo said Schmidt would serve as chair of a commission that'll be tasked with updating the state's  technological infrastructure and practices during and after [the coronavirus pandemic](https://www.cnet.com/how-to/coronavirus-and-covid-19-all-your-questions-answered/). The group will tackle subjects including telehealth, internet broadband and remote learning, Schmidt said. The appointment [also prompted concerns](https://theintercept.com/2020/05/08/andrew-cuomo-eric-schmidt-coronavirus-tech-shock-doctrine/) about the influence of big tech in the public sector, especially given Google's past data privacy scandals. 

Schmidt, 65, joined Google after serving as CEO of software maker Novell. He was introduced to Page and Brin by two of Google's most prominent backers  at that time, venture capitalists John Doerr of Kleiner Perkins and Mike Moritz of Sequoia Capital. During Schmidt's tenure, the company  expanded beyond its roots as a search engine to tackle other  technologies, including [mobile phones](https://www.cnet.com/news/best-phones-of-2019-iphone-11-galaxy-note-10/) and on-line video. It also adopted a corporate structure that reflected  its growing financial success. Schmidt helped take the company public in 2004, a stock [market debut](https://www.cnet.com/news/4-ways-google-changed-tech-ipos-and-the-tech-world-itself/) that made him a billionaire. (He still holds about $5.3 billion in the company's stock.) 

Schmidt remained Google's CEO for a decade, before he moved into his role as  executive chairman and Page took over the top job. In 2015, the company  unveiled a bombshell restructuring, creating a parent company for  Google, called Alphabet. Schmidt became executive chairmen of the new  entity as well. 

In 2017, Schmidt transitioned to the new  position of technical adviser, a job the company never clearly  described. Still, he had two administrative assistants at Google's  headquarters. The staff have been reassigned since Schmidt's departure,  the person familiar with the situation said. 

Schmidt's exit means he is officially, if symbolically, off Alphabet's payroll. He was earning $1 per year in the adviser role. 

> ![gettyimages-51192767](https://cnet4.cbsistatic.com/img/J8qs3uWeSAaJWDGeKRDw982jcL8=/1092x0/2020/05/09/e92fbc08-0417-4223-b27a-47aca6021749/gettyimages-51192767.jpg)
>
> Text: Schmidt and Larry Page (center) at Google's 2004 IPO.
> 
> Source: Getty Images                                                

### 'Maybe you shouldn't be doing it in the first place' 

During and beyond his tenure as CEO, Schmidt earned a reputation for courting controversy. In 2005, Google [blacklisted CNET reporters](https://www.cnet.com/news/wanted-at-google-a-few-good-chefs/) after the website published a story about Google's massive trove of user data. [The story](https://www.cnet.com/news/google-balances-privacy-reach-1/) included personal information about then-CEO Schmidt, gleaned through  Google searches, including his net worth and details about being a  Burning Man attendee and amateur pilot. 

Schmidt has also drawn  scrutiny for his views on user data and privacy. In 2009, when asked if  people should be sharing information with Google like a "trusted  friend," [he responded](https://www.eff.org/deeplinks/2009/12/google-ceo-eric-schmidt-dismisses-privacy), "If you have something that you don't want anyone to know, maybe you shouldn't be doing it in the first place." 

More recently, Schmidt drew push-back for comments around big tech and the  pandemic. In a virtual event with the Economic Club of New York in  April, Schmidt criticized the US' response to the COVID-19 crisis,  saying the government was slow to organize. He then touted Silicon  Valley's role in helping people get information and communicate during  the crisis and said Americans should be "grateful" that tech companies  were able to get funding and help people. 

"The benefit of these  corporations, which we love to malign, in terms of the ability to  communicate, the ability to deal with health, the ability to get  information, is profound," he said [during the discussion](https://www.youtube.com/watch?v=XtAyGVuRQME). "Think about what your life would be like in America without [Amazon](https://www.cnet.com/tags/amazon/)." 