# The Green New Deal Is the Key to Ending Forever Wars

> Autore: [Lindsay Koshgarian](https://truthout.org/authors/lindsay-koshgarian/)
>
> Link: https://truthout.org/articles/the-green-new-deal-is-the-key-to-ending-forever-wars/
>
> Fonte: Truthout
>
> Data di pubblicazione: May 8, 2020
>
> Traduzione: Laura Matilde Mannino
>
> Revisione:
>
> Adattamento:
>
> Link su PeaceLink: 

![Pentagon gas tank in overgrowth with wind power growing](https://truthout.org/wp-content/uploads/2020/05/2020_0508-pentagon-gascan-1200x900.jpg)

The fossil fuel industry is a current casualty of the corona-virus pandemic, with oil prices briefly dipping [below zero at the end of April](https://www.cnn.com/2020/04/28/investing/premarket-stocks-trading/index.html). With the oil industry on the ropes, [progressives see a path](https://truthout.org/articles/a-big-oil-bailout-would-be-the-opposite-of-the-green-new-deal-we-need/) toward a [green economic renewal](https://truthout.org/articles/dont-bail-out-oil-and-gas-use-the-money-for-environmental-cleanup-instead/). Could that spell a whole new approach to international conflict and the US military endeavor, too?

Oil is the leading cause of interstate wars, but the connections  between war and oil don’t stop there. From the Pentagon’s fossil fuel  emissions to militarized responses to climate refugees, the US  military endeavor and our dependence on fossil fuels are intricately  tied, as Lorah Steichen and I explain in a [new report.](https://www.nationalpriorities.org/analysis/2020/no-warming-no-war/) Recognizing those ties could be the key to a whole new world.

## The Leading Cause of War

Oil is a leading cause of war, with [one quarter to one half](https://www.belfercenter.org/publication/oil-conflict-and-us-national-interests) of all interstate wars since 1973 linked to oil. The fact of US war  for oil is such an open secret that in 2008, retired Gen. John Abizaid [said of the Iraq War](https://www.huffpost.com/entry/abizaid-of-course-its-abo_n_68568), “Of course it’s about oil, we can’t really deny that… We’ve treated the Arab world as a big collection of gas stations.”

Today, the US is in its 17th year of war in Iraq. In the ongoing  quest for dominance over the world’s oil supply, the Trump  administration has a target on Iran as well. It seems like years since  January, when the US spontaneously assassinated Iranian Major Gen. [Qassim Suleimani](https://truthout.org/articles/noam-chomsky-us-is-a-rogue-state-and-suleimanis-assassination-confirms-it/), which led to a brief escalation over a period of days that many feared  could spill over to full-fledged war. More recently, the US has  continued to provoke Iran not only with [deadly economic sanctions](https://www.commondreams.org/views/2019/06/17/us-sanctions-economic-sabotage-deadly-illegal-and-ineffective), but naval [incursions](https://www.aljazeera.com/news/2020/04/iranian-vessels-dangerously-close-military-ships-200415214212847.html) into the Persian Gulf and constant verbal [threats](https://www.nytimes.com/2020/04/22/world/middleeast/iran-trump-navy-persian-gulf-satellite.html).

Even the Pentagon’s routine operations are driven by oil interests. One study estimated that the Pentagon spends at minimum [$81 billion a year](https://secureenergy.org/report/military-cost-defending-global-oil-supplies/) — nearly 10 times the budget of the Environmental Protection Agency — to defend the world’s oil supply, in addition to billions spent each year on the Iraq War.

# Climate Change Is a Planetary Emergency

Much like the pandemic, climate change is a global problem, not a  national one. True U.S. leadership on climate wouldn’t just clean up  US emissions, it would open new diplomatic efforts to reduce emissions worldwide — an effort that would be incompatible with war.

> The Pentagon spends at minimum $81 billion a year to defend the world’s  oil supply, in addition to billions spent each year on the Iraq War.
>
> BOX

The Trump administration has a terrible track record on diplomacy,  however. The list of significant international agreements and groups  this administration has abandoned — [the Paris Agreement](https://www.npr.org/2019/11/04/773474657/u-s-formally-begins-to-leave-the-paris-climate-agreement) on climate change, the [Iran nuclear deal](https://www.nytimes.com/2018/05/08/world/middleeast/trump-iran-nuclear-deal.html), the [Intermediate-Range Nuclear Forces Treaty](https://www.armscontrol.org/act/2019-09/news/us-completes-inf-treaty-withdrawal), the [UN Human Rights Council](https://www.vox.com/2018/6/19/17479356/nikki-haley-un-human-rights-council-us-out), [and more](https://www.cnn.com/2019/02/01/politics/nuclear-treaty-trump/index.html) — portends a dangerous inability to negotiate the kind of deals that a  robust response to climate change would require. Most recently, the  president has said that he will [withdraw U.S. funding](https://truthout.org/articles/trumps-defunding-of-the-who-threatens-to-make-the-pandemic-worse/) for the multinational World Health Organization during a once-in-a-century pandemic.

Perhaps most dangerously for climate change, the president and the Pentagon have been [beating the war drums](https://www.defensenews.com/opinion/commentary/2020/03/19/espers-dark-vision-for-us-china-conflict-makes-war-more-likely/) about China since at least [December 2017](https://www.reuters.com/article/us-usa-military-china-russia/u-s-military-puts-great-power-competition-at-heart-of-strategy-mattis-idUSKBN1F81TR), with the Pentagon announcing its intention to make military competition with China and Russia its [main priority](https://www.defensenews.com/pentagon/2019/03/14/in-testimony-shanahan-underlines-its-china-china-china/). As the [world leader](https://climateactiontracker.org/countries/china/) in total climate emissions, China is a make-or-break participant in any international effort to arrest climate change. The U.S. can hardly  afford a Cold War-style communications lockout with China if it intends  to get serious about climate.

## The Gas-Guzzling Pentagon

According to [a recent study](https://watson.brown.edu/costsofwar/papers/ClimateChangeandCostofWar) from Brown University’s Costs of War Project, the Pentagon is the  world’s largest institutional user of petroleum. If the Pentagon were a  country, its emissions would exceed those of industrialized nations like Denmark, Sweden and Portugal.

There can be no US military as it currently exists without oil. The Pentagon has an entire program, the Petroleum Laboratory Training  Division, devoted exclusively to ensuring the quality of the military  fuel. As the program’s [website proclaims](https://quartermaster.army.mil/pwd/pwd_lab.html), fuel is the “blood of the military.” [Gen. David Petraeus](https://www.thenation.com/article/archive/americas-generals-learned-nothing-petraeus-iraq-war/), a commander of the Iraq and Afghanistan wars, [echoed](https://archive.defense.gov/news/newsarticle.aspx?id=64316) the dark, oil-as-blood metaphor, noting that energy “is the lifeblood of our war-fighting capabilities.”

> If the Pentagon were a country, its emissions would exceed those of industrialized nations like Denmark, Sweden and Portugal.
>
> BOX

The largest fighting force on Earth runs on oil. The Pentagon has [800 military installations in 90 countries](https://www.overseasbases.net/fact-sheet.html) and territories across the globe, according to American University  anthropologist David Vine. Running the Pentagon’s domestic and overseas  bases, supplying them and providing a constantly rotating cast of troops accounts for about [40 percent of the Department of Defense’s greenhouse gas emissions.](https://watson.brown.edu/costsofwar/files/cow/imce/papers/Pentagon%20Fuel%20Use,%20Climate%20Change%20and%20the%20Costs%20of%20War%20Revised%20November%202019%20Crawford.pdf)

Another major source of emissions is the air-strikes that are a [hallmark of modern U.S. military engagement](https://www.newyorker.com/news/news-desk/the-recent-history-of-bombing-the-shit-out-of-em). Just one of the military’s jets, the B-52 Stratofortress, [consumes about as much fuel](https://www.tni.org/en/article/climate-change-capitalism-and-the-military) in an hour as the average car driver uses in seven years. The military  campaign against ISIS (also known as Daesh), which began in 2014, has  involved [tens of thousands](https://watson.brown.edu/costsofwar/files/cow/imce/papers/Pentagon%20Fuel%20Use%2C%20Climate%20Change%20and%20the%20Costs%20of%20War%20Revised%20November%202019%20Crawford.pdf) of aerial sorties ranging from weapons strikes to airlifts and  reconnaissance. The fuel use of those missions can be jaw-dropping: In  2014, [two B-2 bombers](https://theaviationist.com/2017/01/20/all-we-know-about-the-u-s-b-2-bombers-30-hour-round-trip-mission-to-pound-daesh-in-libya/) flew a mission from Missouri to drop bombs on ISIS targets in Libya — a 30-hour round trip that required the use of two different types of aerial refueling tankers and more than 400 tons of fuel.

The Pentagon has proclaimed that reducing fuel usage is a goal — if only to further bulletproof their own missions by [reducing dependence on vulnerable supply lines](https://media.nationalpriorities.org/uploads/publications/no_warming,_no_war_-_climate_militarism_primer.pdf). Some military critics’ [proposals](https://truthout.org/articles/the-us-military-wont-lead-the-fight-in-combating-climate-change/) have called for various means of “greening the military.” Ultimately,  attempts to “green the military” won’t meaningfully contribute to a  green future — both because the Pentagon’s activities are [inherently high-emission](https://media.nationalpriorities.org/uploads/publications/no_warming,_no_war_-_climate_militarism_primer.pdf), but also because the Green New Deal’s commitment to human life and self-determination is incompatible with US militarism.

## A Just (and Peaceful) Transition

The concept of a [“just transition”](https://climatejusticealliance.org/just-transition/) that builds an economy based on health and thriving communities rather  than exploitation and harm is a key moral holding of the climate  movement.

> Just one of the military’s jets, the B-52 Stratofortress, consumes about as much fuel in an hour as the average car driver uses in seven years.
>
> BOX

That concept is incompatible with environmental destruction. It compels restitution for the many [Indigenous peoples](https://www.overseasbases.net/fact-sheet.html) whose [lives and lands](https://media.nationalpriorities.org/uploads/publications/no_warming,_no_war_-_climate_militarism_primer.pdf) have been destroyed. A just transition must stop the burning of  military waste in Iraq, which has contributed to widespread toxicity and elevated rates of cancer and infants born with tumors and missing  limbs, that have been [described as](https://media.nationalpriorities.org/uploads/publications/no_warming,_no_war_-_climate_militarism_primer.pdf) “the highest rate of genetic damage to any population ever studied.”

Climate and war refugees must be promised safe harbor. One estimate  shows that by mid-century, climate change could force the relocation of [200 million people](https://publications.iom.int/books/mrs-ndeg31-migration-and-climate-change). Already, climate change has been [a factor](https://publications.iom.int/books/mrs-ndeg31-migration-and-climate-change) in forced migration around the world, including in the devastating  Syrian War and in rising migration to the United States from Guatemala,  Honduras and El Salvador.

These migration flows are being met with militarized responses. In  the US., the fight over the southern border wall is a harbinger of  worse to come. An ominous 2003 Pentagon report on climate change [foretold](https://books.google.com/books?id=8a5W04dDHTwC&pg=PA18&lpg=PA18&dq=United+States+and+Australia+are+likely+to+build+defensive+fortresses+around+their+countries+because+they+have+the+resources+and+reserves+to+achieve+self-sufficiency&source=bl&ots=ASVGf1pqDu&sig=ACfU3U0oyU0f1z3EgtI9UhiMD5xEpzy8VQ&hl=en&sa=X&ved=2ahUKEwiFyJDi74vpAhWjTN8KHboyB2MQ6AEwAHoECAcQAQ) that the “United States [is] likely to build defensive fortresses  around [its country] because they have the resources and reserves to  achieve self-sufficiency…. Borders will be strengthened around the  country to hold back unwanted starving immigrants from the Caribbean  islands (an especially severe problem), Mexico, and South America.”

The militarized response to climate change extends inward from the borders. Aggressive deportation campaigns, [internment of undocumented people](https://derechoshumanosaz.net/coalition-work/the-criminalization-of-migration/) by Immigration and Customs Enforcement, and the [violent response](https://www.thenation.com/article/archive/militarized-police-are-cracking-down-on-dakota-access-pipeline-protesters/) to climate and environmental protests may seem unrelated, but the need  of powerful forces to subdue the effects and response to climate change  is a driving force behind each of them. As activists and experts design  policies to end our dependence on oil, the just transition principle  suggests that the solution must also dismantle the web of related  injustice.

## How Do We Pay for It?

The Green New Deal will indeed require extensive resources, but those same resources have been propping up U.S. wars for nearly two decades  now. While the US is on the hook for [$6.4 trillion](https://watson.brown.edu/costsofwar/files/cow/imce/papers/2019/US%20Budgetary%20Costs%20of%20Wars%20November%202019.pdf) for the Iraq and Afghanistan wars, the cost of converting the US energy grid to 100 percent renewable sources would be only [$4.5 trillion](https://www.woodmac.com/news/feature/deep-decarbonisation-the-multi-trillion-dollar-question/?utm_source=gtmarticle&utm_medium=web&utm_campaign=wmpr_griddecarb). The cost of missing the opportunity to convert the energy grid  beginning in 2001 instead of embarking on two decades of war is  incalculable.

> Dollar for dollar, clean energy creates 40 percent more jobs than military spending.
>
> BOX

Today, the US is still spending \$70 billion each year on its forever wars. That money could be re-purposed — along with additional cuts to Pentagon spending that would total [$350 billion](https://media.nationalpriorities.org/uploads/publications/ppc-moral-budget-2019-report-full-final-3.pdf) per year — toward a Green New Deal. These cuts would allow for the diversion of  resources, but they would also directly reduce emissions and reduce  conflict to make the world a safer place. Another [$649 billion](https://www.imf.org/en/Publications/WP/Issues/2019/05/02/Global-Fossil-Fuel-Subsidies-Remain-Large-An-Update-Based-on-Country-Level-Estimates-46509) per year could be freed up by ending US oil industry subsidies. All  told, that’s a trillion dollars a year that could go toward a Green New  Deal.

Some of the most profound (and sympathy-inducing) opposition to both a Green New Deal and to cutting Pentagon spending comes from people who  now depend on the oil and military industries for their livelihoods. A  key part of a just transition includes ensuring that those people can  transition from [dangerous jobs](https://www.marketplace.org/2015/09/18/working-oil-fields-dangerous/) that fuel suffering and conflict, to safer, life-affirming jobs. This  transition is eminently possible. Dollar for dollar, clean energy  creates [40 percent more jobs40 percent more jobs](https://watson.brown.edu/costsofwar/files/cow/imce/papers/2017/Job%20Opportunity%20Cost%20of%20War%20-%20HGP%20-%20FINAL.pdf) than military spending. A relatively modest $200 billion annual investment in clean energy — far less than what the Green New Deal proposes — would create [2.7 million net new jobs.](http://www.peri.umass.edu/fileadmin/pdf/Green_Growth_2014/GreenGrowthReport-PERI-Sept2014.pdf)

Dismantling the hold of the oil and military industries — and the colonialist mindset of the 20th century — won’t be easy, but it is a necessary part of the solution to the  challenges the world now faces. As the world emerges from its forced  hibernation, the price of oil should be one thing that doesn’t come  back.